CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Maintainers

INTRODUCTION
------------
This module brings some facilities to access content entity values with a system
of fallback. For example, we can add a facultative field that allows user to
override the default obligatory title of an entity. EntittyFallbackValues will
allow you to define priority and access the value like that :

```php
$fallbackValuesManager = \Drupal::service(EntityFallbackValueManager::SERVICE_NAME);
$definitions = [
  'title' => [
    'field_override_title.value',
    'title.value',
  ],
];
$value = $fallbackValuesManager->getEntityCustomFallbackValues($myEntity, $definitions);
```

Of course this example is a bit heavy, but it is really usefull when you have
several override fields.

You can also create a plugin that will define the default definition for a
specific entity type, or a specific bundle of an entity type that will then
facilitate the access to this fallbak values :

This plugin will only applies on node of "page" bundle :

```php
/**
 * Plugin implementation for the entity value fallback.
 *
 * @EntityFallbackValuePluginAnnotation(
 *   id = "node",
 *   applies_on = { "node.page" }
 * )
 */
class Node extends AbstractEntityFallbackValuePlugin implements EntityFallbackValuePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityFallbackDefinitions(): array {
    return [
      'title'       => [
        'field_override_title',
        'title',
      ],
      'description' => [
        'paragraph_thumbnail.field_description',
        'field_description',
      ],
    ];
  }
}
```

Then you can easily access the data :

```php
$values = $fallbackValuesManager->getEntityFallbackValues($node);
```

To create a plugin, plaese use the `drush generate efp` command.

REQUIREMENTS
------------

This module has no specific requirements

INSTALLATION
------------

Install the Env Libraries module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

MAINTAINERS
-----------

* Thomas Sécher - https://www.drupal.org/u/tsecher
