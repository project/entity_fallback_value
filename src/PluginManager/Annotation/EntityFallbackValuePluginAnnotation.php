<?php

namespace Drupal\entity_fallback_value\PluginManager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a Plugin annotation object.
 *
 * @Annotation
 *
 * @ingroup
 */
class EntityFallbackValuePluginAnnotation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Applies on : list of entity_type_id.bundle.
   *
   * @var array
   */
  public $applies_on = [];

}
