<?php

namespace Drupal\entity_fallback_value\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class TreePluginManager.
 *
 * @package Drupal\entity_fallback_value\PluginManager
 */
class EntityFallbackValuePluginManager extends DefaultPluginManager {

  /**
   * Service ID.
   *
   * @const string
   */
  const SERVICE_NAME = 'entity_fallback_value.plugin_manager';

  /**
   * Package name.
   *
   * @const string
   */
  const PACKAGE_NAME = 'entity_fallback_value';

  /**
   * Instances cache.
   *
   * @var \Drupal\entity_fallback_value\PluginManager\Plugin\EntityFallbackValuePluginInterface[]|null
   */
  protected $instances = NULL;

  /**
   * Singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/' . static::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      'Drupal\\' . static::PACKAGE_NAME . '\PluginManager\Plugin\EntityFallbackValuePluginInterface',
      'Drupal\\' . static::PACKAGE_NAME . '\PluginManager\Annotation\EntityFallbackValuePluginAnnotation'
    );

    $this->alterInfo(static::PACKAGE_NAME . '_info');
  }

  /**
   * Return the plugins that apply on $contentEntityBase.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity_base
   *   The base entity.
   *
   * @return array
   *   The list of plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getInstancesByContentEntity(ContentEntityInterface $content_entity_base): array {
    $instances = [];
    foreach ($this->getInstances() ?? [] as $instance) {
      if ($instance->applies($content_entity_base)) {
        $instances[] = $instance;
      }
    }

    return $instances;
  }

  /**
   * Get the list of plugin instances.
   *
   * @return \Drupal\entity_fallback_value\PluginManager\Plugin\EntityFallbackValuePluginInterface[]|null
   *   The instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getInstances() {
    if (is_null($this->instances)) {
      foreach ($this->getDefinitions() as $definition) {
        $plugin_id = $definition['id'];
        $instance = parent::createInstance($plugin_id, $this->getAppliesOnArray($definition['applies_on']));
        $this->instances[$plugin_id] = $instance;
      }
    }

    return $this->instances;
  }

  /**
   * Return an array of entity_type_id => [bundles].
   *
   * @param array $applies_on
   *   THe definition values.
   *
   * @return array
   *   THe applies on array.
   */
  protected function getAppliesOnArray(array $applies_on) {
    $applies_on_array = [];

    foreach ($applies_on as $value) {
      [$entityTypeId, $bundle] = explode('.', $value . '.');
      if (!isset($applies_on_array[$entityTypeId])) {
        $applies_on_array[$entityTypeId] = [];
      }
      if (!empty($bundle)) {
        $applies_on_array[$entityTypeId][] = $bundle;
      }
    }

    return $applies_on_array;
  }

}
