<?php

namespace Drupal\entity_fallback_value\PluginManager\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for Entity Fallbak values plugin.
 *
 * @package Drupal\entity_fallback_value\PluginManager\Plugin
 */
interface EntityFallbackValuePluginInterface {

  /**
   * REturn the entity fallback values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   THe content entity.
   *
   * @return mixed
   *   The values.
   */
  public function getEntityFallbackValues(ContentEntityInterface $content_entity = NULL);

  /**
   * Return the list of fallback definitions.
   *
   * @return array
   *   THe defintions.
   */
  public function getEntityFallbackDefinitions(): array;

  /**
   * Return true if the plugin applies for the content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $content_entity
   *   The content entity.
   *
   * @return bool
   *   The status.
   */
  public function applies(ContentEntityInterface $content_entity = NULL): bool;

}
