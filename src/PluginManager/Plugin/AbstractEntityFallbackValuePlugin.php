<?php

namespace Drupal\entity_fallback_value\PluginManager\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_fallback_value\Traits\AccessNestedFieldsTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Gives applies base method for EntityFallback plugins.
 *
 * @package Drupal\entity_fallback_value\PluginManager\Plugin
 */
abstract class AbstractEntityFallbackValuePlugin implements ContainerFactoryPluginInterface, EntityFallbackValuePluginInterface {
  use AccessNestedFieldsTrait;

  /**
   * AppliesOn configuration.
   *
   * @var array
   */
  protected array $appliesOn;

  /**
   * AbstractEntityFallbackValuePlugin constructor.
   *
   * @param array $applies_on
   *   The configuration for default apply.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(array $applies_on, EntityRepositoryInterface $entity_repository) {
    $this->appliesOn = $applies_on;
    $this->setEntityRepository($entity_repository);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ContentEntityInterface $content_entity = NULL): bool {
    $applies = FALSE;
    $entity_type_id = $content_entity->getEntityTypeId();
    $bunde = $content_entity->bundle();
    if (isset($this->appliesOn[$entity_type_id])) {
      // Applies on every bundle.
      if (empty($this->appliesOn[$entity_type_id])) {
        $applies = TRUE;
      }
      elseif (in_array($bunde, $this->appliesOn[$entity_type_id])) {
        $applies = TRUE;
      }
    }

    return $applies;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFallbackValues(ContentEntityInterface $content_entity = NULL) {
    return $this->getEntityFallbackValuesFromDefintions($content_entity, $this->getEntityFallbackDefinitions());
  }

}
