<?php

namespace Drupal\entity_fallback_value\Traits;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\TypedData\ListInterface;
use Drupal\Core\TypedData\PrimitiveBase;

/**
 * Trait to accesss nested fields.
 *
 * @package Drupal\entity_fallback_value\Traits
 */
trait AccessNestedFieldsTrait {

  /**
   * Entity Repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Set the entityRepository.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function setEntityRepository(EntityRepositoryInterface $entity_repository) {
    $this->entityRepository = $entity_repository;
  }

  /**
   * Return the entity fallback values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $content_entity
   *   The content.
   * @param array $definitions
   *   THe list of definitions.
   *
   * @return array
   *   The values.
   */
  public function getEntityFallbackValuesFromDefintions(ContentEntityInterface $content_entity = NULL, array $definitions = []) {
    $values = [];
    foreach ($definitions as $key => $definition) {
      $values[$key] = $this->getFallbackValue($content_entity, $definition, $key);
    }

    return $values;
  }

  /**
   * Return the list of fallback values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The content entity.
   * @param array $fallback_definition
   *   The fallback definitions.
   * @param string $key
   *   The definition key.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\TypedData\TypedDataInterface|false|mixed|null
   *   The list of fallback values.
   */
  protected function getFallbackValue(ContentEntityInterface $content_entity, array $fallback_definition, string $key = NULL) {
    foreach ($fallback_definition as $definition) {
      try {
        $value = $this->getDefinitionValue($content_entity, $definition, $key);
        if (!empty($value)) {
          return $value;
        }
      }
      catch (\Exception $e) {
        // Mute exception...
      }
    }

    return NULL;
  }

  /**
   * Return the value for a nested field definition.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The content entity.
   * @param mixed $definition
   *   The definition.
   * @param string $key
   *   The definition key.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\TypedData\TypedDataInterface|false|mixed|null
   *   THe value.
   */
  protected function getDefinitionValue(ContentEntityInterface $content_entity, $definition, string $key = NULL) {
    if (is_callable($definition)) {
      // Return callback value.
      $value = $this->getValueFromCallback($content_entity, $definition, $key);
    }
    else {
      $value = $this->getValueFromFieldValue($content_entity, $definition, $key);
    }

    return $value;
  }

  /**
   * Return the fallback value calling a callback.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   THe entity.
   * @param mixed $callback
   *   THe callback.
   * @param string $key
   *   The definition key.
   *
   * @return false|mixed
   *   The value.
   */
  protected function getValueFromCallback(ContentEntityInterface $content_entity, $callback, string $key = NULL) {
    return call_user_func($callback, $content_entity, $key);
  }

  /**
   * Return the value from the entity nested field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The entity.
   * @param mixed $definition
   *   The definition.
   * @param null $key
   *   The definition key.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\TypedData\TypedDataInterface|false|mixed|null
   *   The value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function getValueFromFieldValue(ContentEntityInterface $content_entity, $definition, $key = NULL) {
    $nestedField = explode('.', $definition);

    return $this->getFieldValue($content_entity, $nestedField);
  }

  /**
   * Return the field value.
   *
   * @param mixed $parent_entity
   *   The parent entity.
   * @param array $nested_fields
   *   The list of nested fields.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\TypedData\TypedDataInterface|false|mixed|null
   *   THe value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getFieldValue($parent_entity, array $nested_fields = []) {
    if (count($nested_fields) > 0) {
      $field = reset($nested_fields);
      $nextFields = array_slice($nested_fields, 1);
      if ($parentField = $this->getFieldItemValue($parent_entity->get($field))) {
        if (count($nextFields)) {
          return $this->getFieldValue($parentField, $nextFields);
        }
        else {
          return $this->getFieldItemValue($parentField);
        }
      }
    }

    return NULL;
  }

  /**
   * Get field item value.
   *
   * @param mixed $fieldItem
   *   The field item.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\TypedData\TypedDataInterface|false|mixed|null
   *   The value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getFieldItemValue($fieldItem) {
    if ($fieldItem instanceof EntityReferenceFieldItemListInterface) {
      $entities = $fieldItem->referencedEntities();
      if ($this->entityRepository && $this->entityRepository instanceof EntityRepositoryInterface) {
        foreach ($entities as $key => $entity) {
          $entities[$key] = $this->entityRepository->getTranslationFromContext($entity);
        }
      }

      return reset($entities);
    }
    if ($fieldItem instanceof ListInterface) {
      return $fieldItem->first();
    }
    if ($fieldItem instanceof EntityReferenceFieldItemListInterface) {
      return $fieldItem->referencedEntities();
    }
    if ($fieldItem instanceof FieldItemInterface) {
      $value = $fieldItem->getValue();

      return $value;
    }
    if ($fieldItem instanceof PrimitiveBase) {
      return $fieldItem->getValue();
    }

    return $fieldItem;
  }

  /**
   * Add a fallback definition to the definitions list.
   *
   * @param array $itemDefinitions
   *   The definitions list.
   * @param mixed $definition
   *   The definition.
   * @param int $weight
   *   The weight.
   */
  public function addDefinition(array &$itemDefinitions, $definition, int $weight = 0): void {
    array_splice($itemDefinitions, $weight, 0, [$definition]);
  }

}
