<?php

namespace Drupal\entity_fallback_value\Service;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\entity_fallback_value\PluginManager\EntityFallbackValuePluginManager;
use Drupal\entity_fallback_value\Traits\AccessNestedFieldsTrait;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

/**
 * Service allowing to get fallback values for a content entity.
 *
 * @package Drupal\entity_fallback_value\Service
 */
class EntityFallbackValueManager extends AbstractExtension implements ExtensionInterface {
  use AccessNestedFieldsTrait;

  /**
   * EntityFallbackValue Plugin Manager.
   *
   * @var \Drupal\entity_fallback_value\PluginManager\EntityFallbackValuePluginManager
   */
  protected EntityFallbackValuePluginManager $fallbackValuesPluginManager;

  /**
   * Current langcode.
   *
   * @var string
   */
  protected ?string $currentLangCode;

  /**
   * TwigEntityFallbackValueTools constructor.
   *
   * @param \Drupal\entity_fallback_value\PluginManager\EntityFallbackValuePluginManager $fallback_values_plugin_manager
   *   The fallbackValuesPluginManager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The Language manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    EntityFallbackValuePluginManager $fallback_values_plugin_manager,
    LanguageManagerInterface $language_manager,
    EntityRepositoryInterface $entity_repository,
  ) {
    $this->fallbackValuesPluginManager = $fallback_values_plugin_manager;
    $this->currentLangCode = $language_manager->getCurrentLanguage()->getId();
    $this->setEntityRepository($entity_repository);
  }

  /**
   * Returns the list f available functions.
   *
   * @return array
   *   List of functions.
   */
  public function getFunctions() {
    return [
      new TwigFunction('getEntityFallbackValues', [
        $this,
        'getTwigEntityFallbackValues',
      ]),
    ];
  }

  /**
   * Return the fallback values for a twig entity (current language).
   *
   * @param \Drupal\Core\Access\AccessibleInterface|null $content_entity
   *   The content entity.
   * @param mixed|null $keys
   *   The keys.
   * @param mixed|null $definitions
   *   The list of custom definitions.
   * @param bool $use_current_language
   *   True will use the global language instead of the entity language.
   *
   * @return array
   *   The values.
   */
  public function getTwigEntityFallbackValues(AccessibleInterface $content_entity = NULL, $keys = NULL, $definitions = NULL, $use_current_language = TRUE) {
    $values = [];
    if ($content_entity instanceof ContentEntityInterface) {

      if ($use_current_language) {
        if (!$content_entity->hasTranslation($this->currentLangCode)) {
          return $values;
        }

        $content_entity = $content_entity->getTranslation($this->currentLangCode);
      }
      return $this->getEntityFallbackValues($content_entity, $keys, $definitions);
    }

    return $values;
  }

  /**
   * Return the fallback values.
   *
   * @param \Drupal\Core\Access\AccessibleInterface|null $content_entity
   *   The content entity.
   * @param mixed|null $keys
   *   The keys.
   * @param mixed|null $definitions
   *   The list of custom defintions.
   *
   * @return array
   *   The values.
   */
  public function getEntityFallbackValues(AccessibleInterface $content_entity = NULL, $keys = NULL, $definitions = NULL) {
    $values = [];
    if ($content_entity && $content_entity instanceof ContentEntityInterface) {
      if (is_array($definitions)) {
        // Load fallback values from custom definitions.
        $values = $this->getEntityCustomFallbackValues($content_entity, $definitions, $keys);
      }
      else {
        // Load fallback values from default (plugin) definitions.
        $values = $this->getEntityDefaultFallbackValues($content_entity, $keys);
      }
    }

    return $values;
  }

  /**
   * Return the fallback values according to plugins.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The content entity.
   * @param mixed|null $keys
   *   The keys.
   *
   * @return array
   *   The values.
   */
  public function getEntityDefaultFallbackValues(ContentEntityInterface $content_entity, $keys = NULL) {
    $values = [];
    if (isset($content_entity->fallbackValues)) {
      $values = $content_entity->fallbackValues;
    }
    elseif ($plugins = $this->fallbackValuesPluginManager->getInstancesByContentEntity($content_entity)) {
      // @phpstan-ignore-next-line
      $content_entity->fallbackValues = [];
      foreach ($plugins as $plugin) {
        $content_entity->fallbackValues = array_merge(
          $content_entity->fallbackValues,
          $plugin->getEntityFallbackValues($content_entity));
      }

      $values = $content_entity->fallbackValues;
    }

    return $this->getOnlyKeys($values, $keys);
  }

  /**
   * Return the fallback values according to definition.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The content entity.
   * @param array $definitions
   *   The definitions.
   * @param mixed|null $keys
   *   The keys.
   *
   * @return array
   *   The values.
   */
  public function getEntityCustomFallbackValues(ContentEntityInterface $content_entity, array $definitions = [], $keys = NULL) {
    $values = $this->getEntityFallbackValuesFromDefintions($content_entity, $definitions);

    return $this->getOnlyKeys($values, $keys);
  }

  /**
   * Return only the wanted values.
   *
   * @param array $values
   *   The values.
   * @param mixed|null $keys
   *   The wanted keys.
   *
   * @return array
   *   The filtered values.
   */
  protected function getOnlyKeys(array $values, $keys = NULL) {
    if ($keys) {
      $keys = is_array($keys) ? $keys : [$keys];
      $values = array_intersect_key($values, array_flip($keys));
    }

    return $values;
  }

}
