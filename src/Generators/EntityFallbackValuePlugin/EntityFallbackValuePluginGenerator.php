<?php

namespace Drupal\entity_fallback_value\Generators\EntityFallbackValuePlugin;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use DrupalCodeGenerator\Command\Plugin\PluginGenerator;
use Symfony\Component\Console\Question\Question;

/**
 * Class ConfigEntityClonerProcessorPluginGenerator.
 *
 * @package Drupal\config_entity_cloner\Generators
 */
class EntityFallbackValuePluginGenerator extends PluginGenerator {

  /**
   * The command name.
   *
   * @var string
   */
  protected string $name = 'entity-fallback-plugin';

  /**
   * The description.
   *
   * @var string
   */
  protected string $description = 'Generates a EntityFallbackValues plugin.';

  /**
   * The alias.
   *
   * @var string
   */
  protected string $alias = 'efp';

  /**
   * The template dir path.
   *
   * @var string
   */
  protected string $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * ConfigEntityClonerProcessorPluginGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info manager.
   * @param string|null $name
   *   The name.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $bundle_info,
    ?string $name = NULL,
  ) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  protected function collectDefault(array &$vars): void {
    parent::collectDefault($vars);
    $vars['applies_on'] = $this->askForAppliesOn();
  }

  /**
   * Ask applies on question.
   *
   * @return array
   *   The user selection.
   */
  protected function askForAppliesOn(): array {
    // List of entity types.
    $entity_types = array_keys($this->entityTypeManager->getDefinitions());

    $applies_on = [];
    while (TRUE) {
      $question = new Question('Entity Types : Enter the type of entity (or use arrows up/down) that can apply the plugin. Press enter to continue or select all entity types');
      $question->setAutocompleterValues($entity_types);
      $entity_type = $this->io()->askQuestion($question);
      if (!$entity_type) {
        break;
      }
      $applies_on[$entity_type] = [];

      while (TRUE) {
        $question = new Question('Entity Bundles : Type the bundle of ' . $entity_type . ' (or use arrows up/down). Press enter to continue or select all bundles');
        $question->setAutocompleterValues($this->getEntityBundle($entity_type));
        $bundle = $this->io()->askQuestion($question);
        if (!$bundle) {
          break;
        }
        $applies_on[$entity_type][] = $bundle;
      }
    }

    // Flatten the "apply on" values.
    $flatten = [];
    foreach ($applies_on as $entity_type_id => $bundles) {
      if (empty($bundles)) {
        $flatten[] = $entity_type_id;
      }
      foreach ($bundles as $bundle) {
        $flatten[] = $entity_type_id . '.' . $bundle;
      }
    }
    return array_map(fn($value) => '"' . $value . '"', $flatten);
  }

  /**
   * Return the list of available bundles for an entity type.
   *
   * @param string $entity_type_id
   *   The entity type.
   *
   * @return int[]|string[]
   *   THe list of bundles.
   */
  protected function getEntityBundle(string $entity_type_id): array {
    return array_keys($this->bundleInfo->getBundleInfo($entity_type_id));
  }

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $this->collectDefault($vars);

    $this->addFile(
      'src/Plugin/entity_fallback_value/' . $vars['class'] . '.php',
      'templates/plugin.twig'
    );

  }

}
